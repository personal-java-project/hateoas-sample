package com.haris.example.hateoas.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String exception) {
        super(exception);
    }
}

package com.haris.example.hateoas.repository;

import com.haris.example.hateoas.model.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RepositoryRestResource(path = "devices2", collectionResourceRel = "devices3")
public interface DeviceRepository extends JpaRepository<Device, Integer> {

    Optional<Device> findByDnsDevice_DnsDeviceId(Integer dnsDeviceId);
    Optional<Device> findByAgentDevice_AgentDeviceId(Integer agentDeviceId);
}

package com.haris.example.hateoas.repository;

import com.haris.example.hateoas.model.entity.DnsDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DnsDeviceRepository extends JpaRepository<DnsDevice, Integer> {
}

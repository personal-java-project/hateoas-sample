package com.haris.example.hateoas.repository;

import com.haris.example.hateoas.model.entity.DeviceIpAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceIpAddressRepository extends JpaRepository<DeviceIpAddress, Integer> {
}

package com.haris.example.hateoas.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class DeviceIpAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int deviceIpAddressId;

    private String ipv4;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;
}

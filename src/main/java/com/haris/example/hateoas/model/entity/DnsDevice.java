package com.haris.example.hateoas.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class DnsDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int dnsDeviceId;

    private String hostname;
}

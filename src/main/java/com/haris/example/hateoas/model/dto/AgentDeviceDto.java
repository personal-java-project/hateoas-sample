package com.haris.example.hateoas.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgentDeviceDto {
    private Integer agentDeviceId;
    private String externalClientUuid;
}

package com.haris.example.hateoas.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AgentDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int agentDeviceId;

    private String externalClientUuid;
}

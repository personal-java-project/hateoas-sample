package com.haris.example.hateoas.component;

import com.haris.example.hateoas.controller.DeviceController;
import com.haris.example.hateoas.controller.DeviceIpAddressController;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Configuration
public class DeviceIpAddressModelAssembler implements RepresentationModelAssembler<DeviceIpAddressDto, EntityModel<DeviceIpAddressDto>> {

    @Override
    public EntityModel<DeviceIpAddressDto> toModel(DeviceIpAddressDto entity) {

        return EntityModel.of(entity,
                linkTo(methodOn(DeviceIpAddressController.class).getDeviceIpAddress(entity.getDeviceIpAddressId())).withSelfRel(),
                linkTo(methodOn(DeviceController.class).getDevice(entity.getDeviceId())).withRel("device"));
    }
}

package com.haris.example.hateoas.component;

import com.haris.example.hateoas.controller.DeviceController;
import com.haris.example.hateoas.model.dto.DeviceDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class DeviceModelAssembler implements RepresentationModelAssembler<DeviceDto, EntityModel<DeviceDto>> {

    @Override
    public EntityModel<DeviceDto> toModel(DeviceDto entity) {

        EntityModel<DeviceDto> deviceDtoEntityModel = EntityModel.of(entity,
                linkTo(methodOn(DeviceController.class).getDevice(entity.getDeviceId())).withSelfRel().andAffordance(afford(methodOn(DeviceController.class).editDevice(entity.getDeviceId(), null))),
                linkTo(methodOn(DeviceController.class).getAllDevicesPage(null)).withRel("devicesPaged"),
                linkTo(methodOn(DeviceController.class).getAllDevices()).withRel("devices"),
                linkTo(methodOn(DeviceController.class).getDeviceIpAddressByDeviceId(entity.getDeviceId())).withRel("deviceIpAddresses"));

        if (entity.getHostname() != null) {
            deviceDtoEntityModel.add(linkTo(methodOn(DeviceController.class).getDnsDeviceByDeviceId(entity.getDeviceId())).withRel("dnsDevice"));
        }
        if (entity.getUuid() != null) {
            deviceDtoEntityModel.add(linkTo(methodOn(DeviceController.class).getAgentDeviceByDeviceId(entity.getDeviceId())).withRel("agentDevice"));
        }

        return deviceDtoEntityModel;
    }
}

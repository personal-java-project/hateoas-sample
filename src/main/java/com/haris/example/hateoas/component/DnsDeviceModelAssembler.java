package com.haris.example.hateoas.component;

import com.haris.example.hateoas.controller.DnsDeviceController;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class DnsDeviceModelAssembler implements RepresentationModelAssembler<DnsDeviceDto, EntityModel<DnsDeviceDto>> {
    @Override
    public EntityModel<DnsDeviceDto> toModel(DnsDeviceDto entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(DnsDeviceController.class).getDnsDevice(entity.getDnsDeviceId())).withSelfRel(),
                linkTo(methodOn(DnsDeviceController.class).getDeviceByDnsDevice(entity.getDnsDeviceId())).withRel("device")
        );
    }
}

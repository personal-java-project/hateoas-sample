package com.haris.example.hateoas.component;

import com.haris.example.hateoas.controller.AgentDeviceController;
import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Configuration
public class AgentDeviceModelAssembler implements RepresentationModelAssembler<AgentDeviceDto, EntityModel<AgentDeviceDto>> {
    @Override
    public EntityModel<AgentDeviceDto> toModel(AgentDeviceDto entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(AgentDeviceController.class).getAgentDevice(entity.getAgentDeviceId())).withSelfRel(),
                linkTo(methodOn(AgentDeviceController.class).getDeviceByAgentDevice(entity.getAgentDeviceId())).withRel("device"));
    }
}

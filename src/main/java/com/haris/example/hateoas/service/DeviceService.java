package com.haris.example.hateoas.service;

import com.haris.example.hateoas.exception.DataNotFoundException;
import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import com.haris.example.hateoas.model.entity.AgentDevice;
import com.haris.example.hateoas.model.entity.Device;
import com.haris.example.hateoas.model.entity.DeviceIpAddress;
import com.haris.example.hateoas.model.entity.DnsDevice;
import com.haris.example.hateoas.repository.AgentDeviceRepository;
import com.haris.example.hateoas.repository.DeviceRepository;
import com.haris.example.hateoas.repository.DnsDeviceRepository;
import com.haris.example.hateoas.util.EntityToDtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DnsDeviceRepository dnsDeviceRepository;

    @Autowired
    private AgentDeviceRepository agentDeviceRepository;

    public List<DeviceDto> getAllDevices() {
        List<Device> allDevices = deviceRepository.findAll();
        return allDevices.stream().map(EntityToDtoConverter::convertDeviceToDeviceDto).collect(Collectors.toList());
    }

    public Page<DeviceDto> getAllDevicesPages(Pageable pageable) {
        Page<Device> devicePage = deviceRepository.findAll(pageable);
        return devicePage.map(new Function<Device, DeviceDto>() {

            @Override
            public DeviceDto apply(Device device) {
                return EntityToDtoConverter.convertDeviceToDeviceDto(device);
            }
        });
    }

    public DeviceDto getDevice(Integer deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return EntityToDtoConverter.convertDeviceToDeviceDto(device);
    }

    public DnsDeviceDto getDnsDeviceByDeviceId(Integer deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return EntityToDtoConverter.convertDnsDeviceToDnsDeviceDto(device.getDnsDevice());
    }

    public List<DeviceIpAddressDto> getDeviceIpAddressByDeviceId(Integer deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return device.getDeviceIpAddresses().stream().map(EntityToDtoConverter::convertDeviceIpAddressToDeviceIpAddressDto).collect(Collectors.toList());
    }

    public AgentDeviceDto getAgentDeviceByDeviceId(Integer deviceId) {
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return EntityToDtoConverter.convertAgentDeviceToAgentDeviceDto(device.getAgentDevice());
    }

    public DeviceDto addDevice(DeviceDto deviceRequest) {

        AgentDevice agentDevice = null;
        DnsDevice dnsDevice = null;

        if (deviceRequest.getUuid() != null) {
            agentDevice = AgentDevice.builder()
                    .externalClientUuid(deviceRequest.getUuid())
                    .build();
            agentDeviceRepository.save(agentDevice);
        }
        if (deviceRequest.getHostname() != null) {
            dnsDevice = DnsDevice.builder()
                    .hostname(deviceRequest.getHostname())
                    .build();
            dnsDeviceRepository.save(dnsDevice);
        }

        Device device = Device.builder()
                .agentDevice(agentDevice)
                .dnsDevice(dnsDevice)
                .deviceIpAddresses(new ArrayList<>())
                .build();

        Device savedDevice = deviceRepository.save(device);
        return EntityToDtoConverter.convertDeviceToDeviceDto(savedDevice);
    }

    public DeviceDto putDevice(Integer deviceId, DeviceDto deviceRequest) {
        Device selectedDevice = deviceRepository.findById(deviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));

        if (selectedDevice.getDnsDevice() != null) {
            selectedDevice.getDnsDevice().setHostname(deviceRequest.getHostname());
        }

        if (selectedDevice.getAgentDevice() != null) {
            selectedDevice.getAgentDevice().setExternalClientUuid(deviceRequest.getUuid());
        }

        List<DeviceIpAddress> deviceIpAddresses = new ArrayList<>();

        deviceRequest.getIpv4().forEach(s -> {
            DeviceIpAddress deviceIpAddress = DeviceIpAddress.builder()
                    .ipv4(s)
                    .device(selectedDevice)
                    .build();
        });

        selectedDevice.setDeviceIpAddresses(deviceIpAddresses);

        Device device = deviceRepository.save(selectedDevice);
        return EntityToDtoConverter.convertDeviceToDeviceDto(device);
    }

    public void deleteDevice(Integer deviceId) {
        deviceRepository.deleteById(deviceId);
    }
}

package com.haris.example.hateoas.service;

import com.haris.example.hateoas.exception.DataNotFoundException;
import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.entity.AgentDevice;
import com.haris.example.hateoas.model.entity.Device;
import com.haris.example.hateoas.repository.AgentDeviceRepository;
import com.haris.example.hateoas.repository.DeviceRepository;
import com.haris.example.hateoas.util.EntityToDtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AgentDeviceService {
    @Autowired
    private AgentDeviceRepository agentDeviceRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    public AgentDeviceDto getAgentDevice(Integer agentDeviceId) {
        AgentDevice agentDevice = agentDeviceRepository.findById(agentDeviceId).orElseThrow(() -> new DataNotFoundException("Agent device not found"));
        return EntityToDtoConverter.convertAgentDeviceToAgentDeviceDto(agentDevice);
    }

    public DeviceDto getDeviceByAgentDevice(Integer agentDeviceId) {
        Device device = deviceRepository.findByAgentDevice_AgentDeviceId(agentDeviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return EntityToDtoConverter.convertDeviceToDeviceDto(device);
    }
}

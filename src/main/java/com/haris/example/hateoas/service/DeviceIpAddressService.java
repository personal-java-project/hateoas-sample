package com.haris.example.hateoas.service;

import com.haris.example.hateoas.exception.DataNotFoundException;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import com.haris.example.hateoas.model.entity.DeviceIpAddress;
import com.haris.example.hateoas.repository.DeviceIpAddressRepository;
import com.haris.example.hateoas.util.EntityToDtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeviceIpAddressService {

    @Autowired
    private DeviceIpAddressRepository deviceIpAddressRepository;

    public DeviceDto getDeviceByDeviceIpAddress(Integer deviceIpAddressId) {
        DeviceIpAddress deviceIpAddress = deviceIpAddressRepository.findById(deviceIpAddressId).orElseThrow(() -> new DataNotFoundException("Device Ip address not found"));

        return EntityToDtoConverter.convertDeviceToDeviceDto(deviceIpAddress.getDevice());
    }

    public DeviceIpAddressDto getDeviceIpAddress(Integer deviceIpAddressId) {
        DeviceIpAddress deviceIpAddress = deviceIpAddressRepository.findById(deviceIpAddressId).orElseThrow(() -> new DataNotFoundException("Device Ip address not found"));

        return EntityToDtoConverter.convertDeviceIpAddressToDeviceIpAddressDto(deviceIpAddress);
    }
}

package com.haris.example.hateoas.service;

import com.haris.example.hateoas.exception.DataNotFoundException;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import com.haris.example.hateoas.model.entity.Device;
import com.haris.example.hateoas.model.entity.DnsDevice;
import com.haris.example.hateoas.repository.DeviceRepository;
import com.haris.example.hateoas.repository.DnsDeviceRepository;
import com.haris.example.hateoas.util.EntityToDtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DnsDeviceService {

    @Autowired
    private DnsDeviceRepository dnsDeviceRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    public List<DnsDeviceDto> getAllDnsDevices() {
        List<DnsDevice> allDnsDevices = dnsDeviceRepository.findAll();
        return allDnsDevices.stream().map(EntityToDtoConverter::convertDnsDeviceToDnsDeviceDto).collect(Collectors.toList());
    }

    public DnsDeviceDto getDnsDevice(Integer dnsDeviceId) {
        DnsDevice dnsDevice = dnsDeviceRepository.findById(dnsDeviceId).orElseThrow(() -> new DataNotFoundException("DNS Device not found"));
        return EntityToDtoConverter.convertDnsDeviceToDnsDeviceDto(dnsDevice);
    }

    public DeviceDto getDeviceByDnsDevice(Integer dnsDeviceId) {
        Device device = deviceRepository.findByDnsDevice_DnsDeviceId(dnsDeviceId).orElseThrow(() -> new DataNotFoundException("Device not found"));
        return EntityToDtoConverter.convertDeviceToDeviceDto(device);
    }
}

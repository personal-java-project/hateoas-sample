package com.haris.example.hateoas.util;

import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import com.haris.example.hateoas.model.entity.AgentDevice;
import com.haris.example.hateoas.model.entity.Device;
import com.haris.example.hateoas.model.entity.DeviceIpAddress;
import com.haris.example.hateoas.model.entity.DnsDevice;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Collectors;

@Slf4j
public class EntityToDtoConverter {

    public static DeviceDto convertDeviceToDeviceDto(Device device) {

        DeviceDto deviceDto = DeviceDto.builder()
                .deviceId(device.getDeviceId())
                .build();
        if (device.getAgentDevice() != null) {
            deviceDto.setUuid(device.getAgentDevice().getExternalClientUuid());
        }
        if (device.getDnsDevice() != null) {
            deviceDto.setHostname(device.getDnsDevice().getHostname());
        }

        deviceDto.setIpv4(device.getDeviceIpAddresses().stream().map(DeviceIpAddress::getIpv4).collect(Collectors.toList()));
        return deviceDto;
    }

    public static DnsDeviceDto convertDnsDeviceToDnsDeviceDto(DnsDevice dnsDevice) {
        return DnsDeviceDto.builder()
                .dnsDeviceId(dnsDevice.getDnsDeviceId())
                .hostname(dnsDevice.getHostname())
                .build();
    }

    public static DeviceIpAddressDto convertDeviceIpAddressToDeviceIpAddressDto(DeviceIpAddress deviceIpAddress) {
        return DeviceIpAddressDto.builder()
                .deviceIpAddressId(deviceIpAddress.getDeviceIpAddressId())
                .ipv4(deviceIpAddress.getIpv4())
                .deviceId(deviceIpAddress.getDevice().getDeviceId())
                .build();
    }

    public static AgentDeviceDto convertAgentDeviceToAgentDeviceDto(AgentDevice agentDevice) {
        return AgentDeviceDto.builder()
                .agentDeviceId(agentDevice.getAgentDeviceId())
                .externalClientUuid(agentDevice.getExternalClientUuid())
                .build();
    }
}

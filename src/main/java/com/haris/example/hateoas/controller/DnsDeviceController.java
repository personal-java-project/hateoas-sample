package com.haris.example.hateoas.controller;

import com.haris.example.hateoas.component.DeviceModelAssembler;
import com.haris.example.hateoas.component.DnsDeviceModelAssembler;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import com.haris.example.hateoas.model.entity.DnsDevice;
import com.haris.example.hateoas.service.DeviceService;
import com.haris.example.hateoas.service.DnsDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
public class DnsDeviceController {

    @Autowired
    private DnsDeviceService dnsDeviceService;

    @Autowired
    private DnsDeviceModelAssembler dnsDeviceModelAssembler;

    @Autowired
    private DeviceModelAssembler deviceModelAssembler;

    @GetMapping(value = "/dns-devices/{dnsDeviceId}")
    public EntityModel<DnsDeviceDto> getDnsDevice(@PathVariable Integer dnsDeviceId) {
        DnsDeviceDto dnsDeviceDto = dnsDeviceService.getDnsDevice(dnsDeviceId);

        return dnsDeviceModelAssembler.toModel(dnsDeviceDto);
    }
    
    @GetMapping(value = "/dns-devices/{dnsDeviceId}/device")
    public EntityModel<DeviceDto> getDeviceByDnsDevice(@PathVariable Integer dnsDeviceId) {
        DeviceDto deviceDto = dnsDeviceService.getDeviceByDnsDevice(dnsDeviceId);
        return deviceModelAssembler.toModel(deviceDto);
    }
}

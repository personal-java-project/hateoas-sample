package com.haris.example.hateoas.controller;

import com.haris.example.hateoas.component.AgentDeviceModelAssembler;
import com.haris.example.hateoas.component.DeviceIpAddressModelAssembler;
import com.haris.example.hateoas.component.DeviceModelAssembler;
import com.haris.example.hateoas.component.DnsDeviceModelAssembler;
import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import com.haris.example.hateoas.model.dto.DnsDeviceDto;
import com.haris.example.hateoas.model.entity.Device;
import com.haris.example.hateoas.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceModelAssembler deviceModelAssembler;

    @Autowired
    private DnsDeviceModelAssembler dnsDeviceModelAssembler;

    @Autowired
    private AgentDeviceModelAssembler agentDeviceModelAssembler;

    @Autowired
    private DeviceIpAddressModelAssembler deviceIpAddressModelAssembler;

    @Autowired
    private PagedResourcesAssembler<DeviceDto> deviceDtoPagedResourcesAssembler;

    @GetMapping(value = "/devices")
    public ResponseEntity<CollectionModel<EntityModel<DeviceDto>>> getAllDevices() {
        List<EntityModel<DeviceDto>> devicesDto = deviceService.getAllDevices().stream().map(deviceModelAssembler::toModel).collect(Collectors.toList());
        return ResponseEntity.ok(CollectionModel.of(devicesDto,
                linkTo(methodOn(DeviceController.class).getAllDevices()).withSelfRel()
                        .andAffordance(afford(methodOn(DeviceController.class).addDevice(null)))));
    }

    @GetMapping(value = "/devices/paged")
    public ResponseEntity<PagedModel<EntityModel<DeviceDto>>> getAllDevicesPage(Pageable pageable) {
        Page<DeviceDto> devicesDto = deviceService.getAllDevicesPages(pageable);
        PagedModel<EntityModel<DeviceDto>> pagedDevicesDto = deviceDtoPagedResourcesAssembler.toModel(devicesDto, deviceModelAssembler);
        return ResponseEntity.ok(pagedDevicesDto);
    }

    @GetMapping(value = "/devices/{deviceId}")
    public ResponseEntity<EntityModel<DeviceDto>> getDevice(@PathVariable Integer deviceId) {
        DeviceDto deviceDto = deviceService.getDevice(deviceId);

        return ResponseEntity.ok(deviceModelAssembler.toModel(deviceDto));
    }

    @GetMapping(value = "/devices/{deviceId}/dns-device")
    public ResponseEntity<EntityModel<DnsDeviceDto>> getDnsDeviceByDeviceId(@PathVariable Integer deviceId) {
        DnsDeviceDto dnsDeviceDto = deviceService.getDnsDeviceByDeviceId(deviceId);

        return ResponseEntity.ok(dnsDeviceModelAssembler.toModel(dnsDeviceDto));
    }

    @GetMapping(value = "/devices/{deviceId}/agent-device")
    public ResponseEntity<EntityModel<AgentDeviceDto>> getAgentDeviceByDeviceId(@PathVariable Integer deviceId) {
        AgentDeviceDto agentDeviceDto = deviceService.getAgentDeviceByDeviceId(deviceId);

        return ResponseEntity.ok(agentDeviceModelAssembler.toModel(agentDeviceDto));
    }

    @GetMapping(value = "/devices/{deviceId}/dns-ip-addresses")
    public ResponseEntity<CollectionModel<EntityModel<DeviceIpAddressDto>>> getDeviceIpAddressByDeviceId(@PathVariable Integer deviceId) {
        List<EntityModel<DeviceIpAddressDto>> deviceIpAddressDtos = deviceService.getDeviceIpAddressByDeviceId(deviceId).stream().map(deviceIpAddressModelAssembler::toModel).collect(Collectors.toList());
        return ResponseEntity.ok(CollectionModel.of(deviceIpAddressDtos, linkTo(methodOn(DeviceController.class).getDeviceIpAddressByDeviceId(deviceId)).withSelfRel()));
    }

    @PostMapping(value = "/devices")
    public ResponseEntity<?> addDevice(@RequestBody DeviceDto deviceRequest) {
        DeviceDto deviceDto = deviceService.addDevice(deviceRequest);
        EntityModel<DeviceDto> deviceDtoEntityModel = deviceModelAssembler.toModel(deviceDto);
        return deviceDtoEntityModel.getLink(IanaLinkRelations.SELF).map(Link::getHref).map(href -> {
            try {
                return new URI(href);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }).map(uri -> ResponseEntity.noContent().location(uri).build()).orElse(ResponseEntity.badRequest().body("Failed to add device"));
    }

    @PutMapping(value = "/devices/{deviceId}")
    public ResponseEntity<?> editDevice(@PathVariable Integer deviceId, @RequestBody DeviceDto deviceRequest) {
        DeviceDto deviceDto = deviceService.putDevice(deviceId, deviceRequest);
        EntityModel<DeviceDto> deviceDtoEntityModel = deviceModelAssembler.toModel(deviceDto);
        return deviceDtoEntityModel.getLink(IanaLinkRelations.SELF).map(Link::getHref).map(href -> {
            try {
                return new URI(href);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }).map(uri -> ResponseEntity.noContent().location(uri).build()).orElse(ResponseEntity.badRequest().body("Failed to edit device"));
    }
}

package com.haris.example.hateoas.controller;

import com.haris.example.hateoas.component.DeviceIpAddressModelAssembler;
import com.haris.example.hateoas.model.dto.DeviceIpAddressDto;
import com.haris.example.hateoas.service.DeviceIpAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeviceIpAddressController {
    @Autowired
    private DeviceIpAddressService deviceIpAddressService;

    @Autowired
    private DeviceIpAddressModelAssembler deviceIpAddressModelAssembler;

    @GetMapping(value = "/device-ip-addresses/{deviceIpAddressId}")
    public EntityModel<DeviceIpAddressDto> getDeviceIpAddress(@PathVariable Integer deviceIpAddressId) {
        DeviceIpAddressDto deviceIpAddressDto = deviceIpAddressService.getDeviceIpAddress(deviceIpAddressId);
        return deviceIpAddressModelAssembler.toModel(deviceIpAddressDto);
    }
}

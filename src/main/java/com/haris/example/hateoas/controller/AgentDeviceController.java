package com.haris.example.hateoas.controller;

import com.haris.example.hateoas.component.AgentDeviceModelAssembler;
import com.haris.example.hateoas.component.DeviceModelAssembler;
import com.haris.example.hateoas.model.dto.AgentDeviceDto;
import com.haris.example.hateoas.model.dto.DeviceDto;
import com.haris.example.hateoas.service.AgentDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AgentDeviceController {
    @Autowired
    private AgentDeviceService agentDeviceService;

    @Autowired
    private AgentDeviceModelAssembler agentDeviceModelAssembler;

    @Autowired
    private DeviceModelAssembler deviceModelAssembler;

    @GetMapping(value = "/agent-devices/{agentDeviceId}")
    public EntityModel<AgentDeviceDto> getAgentDevice(@PathVariable Integer agentDeviceId) {
        AgentDeviceDto agentDeviceDto = agentDeviceService.getAgentDevice(agentDeviceId);
        return agentDeviceModelAssembler.toModel(agentDeviceDto);
    }

    @GetMapping(value = "/agent-devices/{agentDeviceId}/device")
    public EntityModel<DeviceDto> getDeviceByAgentDevice(@PathVariable Integer agentDeviceId) {
        DeviceDto deviceDto = agentDeviceService.getDeviceByAgentDevice(agentDeviceId);
        return deviceModelAssembler.toModel(deviceDto);
    }
}

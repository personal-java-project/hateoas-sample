insert into agent_device values (1, '12312321-das123123-qweqw123123213');
insert into agent_device values (2, '44561230-jhh234235-po909412312456');

insert into dns_device values (1, 'www.google.com');
insert into dns_device values (2, 'www.amazon.com');

insert into device values (1, 1, null);
insert into device values (2, null, 2);

insert into device_ip_address values (1, '1.1.1.1', 1);

insert into device_ip_address values (4, '1.1.10.1', 2);
insert into device_ip_address values (5, '1.1.10.2', 2);
insert into device_ip_address values (6, '1.1.10.3', 2);